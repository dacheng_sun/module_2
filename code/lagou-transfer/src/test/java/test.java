import com.sun.factory.BeanFactory;
import com.sun.factory.ProxyFactory;
import com.sun.service.TransferService;
import com.sun.service.TransferServiceImpl2;
import org.junit.Test;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/5/3 18:10
 */
public class test {

//    /**
//     * 基于beans.xml
//     * @throws Exception
//     */
//    @Test
//    public void testA() throws Exception {
//        ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
//        System.out.println("proxyFactory = " + proxyFactory);
//        Object transferService1 = BeanFactory.getBean("transferService");
//        System.out.println("transferService1 = " + transferService1);
//        TransferService transferService = (TransferService) proxyFactory.getJdkProxy(transferService1);
//        System.out.println("transferService = " + transferService);
////        transferService.transfer("6029621011001","6029621011000",500);
//    }


    /**
     * 基于扫描包com.sun
     */
    @Test
    public void testB() throws Exception {
        ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("ProxyFactory");

        Object transferServiceBean = BeanFactory.getBean("transferService");
        TransferService transferService = (TransferService) proxyFactory.getProxy(transferServiceBean);
        transferService.transfer("6029621011001", "6029621011000", 100);

//        Object transferServiceBean2 = BeanFactory.getBean("transferService2");
//        TransferServiceImpl2 transferService2 = (TransferServiceImpl2) proxyFactory.getProxy(transferServiceBean2);
//        transferService2.transfer("6029621011001", "6029621011000", 100);
    }

}
