package com.sun.factory;

import com.sun.annotation.Autowired;
import com.sun.annotation.Service;
import com.sun.utils.ClassUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/5/3 14:30
 */
public class BeanFactory {
    //加载xml文件，通过反射生成实例化对象
    private static Map<String, Object> map = new HashMap<>();

    //提供获取对象的方法
    public static Object getBean(String id) {
        return map.get(id);
    }

    static {
        try {

            //获取包下的所有类
            Class[] allClass = ClassUtils.getClasses("com.sun");
            for (Class aClass : allClass) {
                //判断当前class对象是否为接口
                if (!aClass.isInterface()) {

                    //判断是否有service注解
                    boolean classAnnotationPresent = aClass.isAnnotationPresent(Service.class);
                    if (classAnnotationPresent) {
                        String beanName;
                        Service service = (Service) aClass.getAnnotation(Service.class);
                        String value = service.value();
                        if ("".equals(value)) {
                            beanName = aClass.getSimpleName();
                        } else {
                            beanName = value;
                        }
                        Object beanObject = aClass.newInstance();
                        map.put(beanName, beanObject);
//                    }
                    }

                }

            }

            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String id = entry.getKey();
                Object o = entry.getValue();
                Field[] fields = o.getClass().getDeclaredFields();
                for (Field field : fields) {
                    field.setAccessible(true);
                    //判断是否有Autowired注解
                    boolean b = field.isAnnotationPresent(Autowired.class);
                    if (b) {
                        Class<?> type = field.getType();
                        String fieldName = type.getSimpleName();
                        Object fClass = map.get(fieldName);
                        //判断是否为接口的对象或@service注解的value不为空
                        if (fClass == null) {
                            String typeName = type.getName();

                            if (type.isInterface()) {
                                //接口对象
                                Class[] classes = ClassUtils.getClasses(type.getPackageName());
                                List<Class> list = new ArrayList();
                                for (Class aClass : classes) {
                                    if (!aClass.isInterface()) {
                                        Class[] interfaces = aClass.getInterfaces();
                                        for (Class anInterface : interfaces) {
                                            if (typeName.equals(anInterface.getName())) {
                                                list.add(aClass);
                                            }
                                        }
                                    }
                                }
                                //这里待自定义@Qualifier判断
                                if (list.size() == 1) {
                                    Class aClass = list.get(0);
                                    Service service = (Service) aClass.getAnnotation(Service.class);
                                    String value = service.value();
                                    if ("".equals(value)) {
                                        fClass = map.get(aClass.getSimpleName());
                                    } else {
                                        fClass = map.get(value);
                                    }
                                } else {
                                    throw new Exception(typeName + "实现类为零或不唯一");
                                }
                            } else {
                                //有@service注解且value不为空
                                if (type.isAnnotationPresent(Service.class)) {
                                    Service service = type.getAnnotation(Service.class);
                                    String value = service.value();
                                    fClass = map.get(value);
                                }
                            }
                        }
                        //通过set方法注入
                        Method[] methods = o.getClass().getMethods();
                        for (Method method : methods) {
                            if (("set" + fieldName).equalsIgnoreCase(method.getName())) {
                                method.invoke(o, fClass);
                                map.put(id, o);
                            }
                        }
                    }
                }
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
