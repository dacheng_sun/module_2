package com.sun.factory;

import com.sun.annotation.Autowired;
import com.sun.annotation.Service;
import com.sun.annotation.Transactional;
import com.sun.utils.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Service
public class ProxyFactory {

    @Autowired
    private TransactionManager transactionManager;

    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    //当前类是否有注解
    private static boolean flag = false;

    //当前类的父类是否有注解
    private static boolean parentFlag = false;

    //当前抽象方法的实现是否有注解
    private static boolean implementFlag = false;

    public void tets() {
        System.out.println("测试方法");
    }

    public Object getProxy(Object obj) {
        Object o;
        //获取类的所有接口
        Class<?>[] interfaces = obj.getClass().getInterfaces();
        //判断实现类是否有Transactional的注解
        if (obj.getClass().isAnnotationPresent(Transactional.class)) {
            flag = true;
        }
        //类不实现接口采用cglib,反之用Jdk动态代理
        if (interfaces.length == 0) {
            o = getCglibProxy(obj);
            System.out.println("使用cglib动态代理生成代理对象");
        } else {
            o = getJdkProxy(obj);
            System.out.println("使用Jdk动态代理生成对象");
        }
        return o;
    }


    /**
     * Jdk动态代理
     *
     * @param obj 委托对象
     * @return 代理对象
     */
    private Object getJdkProxy(Object obj) {
        // 获取代理对象
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),
                (proxy, method, args) -> {
                    //判断当前接口是否有Transactional的注解
                    if (method.getDeclaringClass().isAnnotationPresent(Transactional.class)) {
                        parentFlag = true;
                    }
                    //判断实现方法是否有注解
                    Method[] declaredMethods = obj.getClass().getDeclaredMethods();
                    for (Method declaredMethod : declaredMethods) {
                        if (declaredMethod.getName().equals(method.getName())) {
                            if (declaredMethod.isAnnotationPresent(Transactional.class)) {
                                implementFlag = true;
                            }
                        }
                    }
                    Object result;
                    //判断是否开启事务
                    if (method.isAnnotationPresent(Transactional.class) || flag || parentFlag || implementFlag) {
                        System.out.println("拥有Transactional注解：当前抽象方法 = " + method.isAnnotationPresent(Transactional.class)
                                + "，接口 = " + parentFlag + "，实现类 = " + flag + "，实现方法 = " + implementFlag);
                        try {
                            // 开启事务(关闭事务的自动提交)
                            transactionManager.setAutoCommit();

                            result = method.invoke(obj, args);

                            // 提交事务

                            transactionManager.commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                            // 回滚事务
                            transactionManager.rollback();

                            // 抛出异常便于上层servlet捕获
                            throw e;

                        }
                    } else {
                        System.out.println("无事务控制");
                        result = method.invoke(obj, args);
                    }


                    return result;
                });

    }


    /**
     * 使用cglib动态代理生成代理对象
     *
     * @param obj 委托对象
     * @return 代理对象
     */
    private Object getCglibProxy(Object obj) {
        return Enhancer.create(obj.getClass(), (MethodInterceptor) (o, method, objects, methodProxy) -> {
            Object result;
            //判断是否开启事务
            if (method.isAnnotationPresent(Transactional.class) || flag) {
                System.out.println("拥有Transactional注解：当前方法 = " + method.isAnnotationPresent(Transactional.class)
                        + "，当前类 = " + flag);
                try {

                    // 开启事务(关闭事务的自动提交)
                    transactionManager.setAutoCommit();

                    result = method.invoke(obj, objects);

                    // 提交事务

                    transactionManager.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    // 回滚事务
                    transactionManager.rollback();

                    // 抛出异常便于上层servlet捕获
                    throw e;

                }
            } else {
                System.out.println("无事务控制");
                result = method.invoke(obj, objects);
            }

            return result;
        });
    }
}
