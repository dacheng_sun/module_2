package com.sun.annotation;

import java.lang.annotation.*;

@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {
    //    默认@Autowired(required=true)，表示注入的时候，该bean必须存在，否则就会注入失败。
    boolean required() default true;
}
