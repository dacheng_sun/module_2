package com.sun.service;

import com.sun.annotation.Transactional;

//@Transactional
public interface TransferService {

//    @Transactional
    void transfer(String fromCardNo, String toCardNo, int money) throws Exception;
}
