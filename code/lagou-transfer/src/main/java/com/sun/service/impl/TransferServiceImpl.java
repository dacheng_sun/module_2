package com.sun.service.impl;


import com.sun.annotation.Autowired;
import com.sun.annotation.Service;
import com.sun.annotation.Transactional;
import com.sun.dao.AccountDao;
import com.sun.pojo.Account;
import com.sun.service.TransferService;

@Service("transferService")
//@Transactional
public class TransferServiceImpl implements TransferService {

//    private AccountDao accountDao = new JdbcAccountDaoImpl();

//     private AccountDao accountDao = (AccountDao) BeanFactory.getBean("accountDao");

    // 最佳状态
    @Autowired
    private AccountDao accountDao;


    // 构造函数传值/set方法传值

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }


//    @Transactional
    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {

//        try{
//            // 开启事务(关闭事务的自动提交)
//            ConnectionUtils.getInstance().getCurrentThreadConn().setAutoCommit(false);
//            TransactionManager.getInstance().setAutoCommit();

        Account from = accountDao.queryAccountByCardNo(fromCardNo);
        Account to = accountDao.queryAccountByCardNo(toCardNo);

        from.setMoney(from.getMoney() - money);
        to.setMoney(to.getMoney() + money);

        accountDao.updateAccountByCardNo(to);
            int c = 1/0;
        accountDao.updateAccountByCardNo(from);
        System.out.println("转账成功");
            /*// 提交事务
//            ConnectionUtils.getInstance().getCurrentThreadConn().commit();
            TransactionManager.getInstance().commit();
        }catch (Exception e) {
            e.printStackTrace();
            // 回滚事务
//            ConnectionUtils.getInstance().getCurrentThreadConn().rollback();
            TransactionManager.getInstance().rollback();
            // 抛出异常便于上层servlet捕获
            throw e;

        }*/


    }
}
