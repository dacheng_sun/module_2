package com.sun.servlet;


import com.sun.factory.BeanFactory;
import com.sun.factory.ProxyFactory;
import com.sun.pojo.Result;
import com.sun.service.TransferService;
import com.sun.service.TransferServiceImpl2;
import com.sun.service.impl.TransferServiceImpl;
import com.sun.utils.JsonUtils;
import com.sun.utils.TransactionManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "transferServlet", urlPatterns = "/transferServlet")
public class TransferServlet extends HttpServlet {

    // 1. 实例化service层对象
//    private TransferService transferService = new TransferServiceImpl();
//    private TransferService transferService = (TransferService) BeanFactory.getBean("transferService");

    // 从工厂获取委托对象（委托对象是增强了事务控制的功能）

    // 首先从BeanFactory获取到proxyFactory代理工厂的实例化对象
//    private TransferService transferService = (TransferService) ProxyFactory.getInstance().getJdkProxy(BeanFactory.getBean("transferService"));

    //jdk
//    private ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("ProxyFactory");
//    private Object transferServiceBean = BeanFactory.getBean("transferService");
//    private TransferService transferService = (TransferService) proxyFactory.getProxy(transferServiceBean);

    //cglib
    private ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("ProxyFactory");
    private Object transferServiceBean2 = BeanFactory.getBean("transferService2");
    private TransferServiceImpl2 transferService = (TransferServiceImpl2) proxyFactory.getProxy(transferServiceBean2);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 设置请求体的字符编码
        req.setCharacterEncoding("UTF-8");

        String fromCardNo = req.getParameter("fromCardNo");
        String toCardNo = req.getParameter("toCardNo");
        String moneyStr = req.getParameter("money");
        int money = Integer.parseInt(moneyStr);

        Result result = new Result();

        try {

            // 2. 调用service层方法
            transferService.transfer(fromCardNo, toCardNo, money);
            result.setStatus("200");
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus("201");
            result.setMessage(e.toString());
        }

        // 响应
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().print(JsonUtils.object2Json(result));
    }
}
