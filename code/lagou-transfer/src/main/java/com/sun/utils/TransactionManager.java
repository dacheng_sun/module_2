package com.sun.utils;

import com.sun.annotation.Autowired;
import com.sun.annotation.Service;

import java.sql.SQLException;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/5/3 15:43
 */
@Service
public class TransactionManager {

    @Autowired
    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

//    private static TransactionManager transactionManage = new TransactionManager();
//
//    private TransactionManager() {
//    }
//
//    public static TransactionManager getInstance() {
//        return transactionManage;
//    }

    public void setAutoCommit() throws SQLException {
//        ConnectionUtils.getInstance().getCurrentThreadConn().setAutoCommit(false);
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }

    public void commit() throws SQLException {
//        ConnectionUtils.getInstance().getCurrentThreadConn().commit();
        connectionUtils.getCurrentThreadConn().commit();
    }

    public void rollback() throws SQLException {
//        ConnectionUtils.getInstance().getCurrentThreadConn().rollback();
        connectionUtils.getCurrentThreadConn().rollback();
    }
}
