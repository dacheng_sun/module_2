package com.sun.utils;

import com.sun.annotation.Service;
import com.sun.annotation.Transactional;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/5/3 15:24
 */
@Service
public class ConnectionUtils {

//    private static ConnectionUtils connectUtils = new ConnectionUtils();
//
//    private ConnectionUtils() {
//    }
//
//    public static ConnectionUtils getInstance() {
//        return connectUtils;
//    }

    private static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    //获取Connection并绑定到当前线程
    public Connection getCurrentThreadConn() throws SQLException {
        Connection connection = threadLocal.get();
        if (connection == null) {
            connection = DruidUtils.getInstance().getConnection();
            threadLocal.set(connection);
        }
        return connection;
    }

}
